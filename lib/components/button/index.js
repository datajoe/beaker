import button from './button.vue';

export default function install(Vue) {
    Vue.component('b-button', button);
}

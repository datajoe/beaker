/* global window */
// Pipette JS library
import 'pipette';

// Pipette CSS library
import 'pipette/dist/pipette.css';

// Components get imported
import button from './components/button/index.js';

// Components get put into the object
const Beaker = {
    button,
};

Beaker.install = (Vue) => {
    // Register components
    for (const component in Beaker) {
        const installer = Beaker[component];

        if (installer && component !== 'install') {
            Vue.use(installer);
        }
    }
};

// Automatically installs the plugin if Vue exists.
// This does NOT work if using a module environment.
if (typeof window !== 'undefined' && window.Vue) {
    window.Vue.use(Beaker);
}

export default Beaker;

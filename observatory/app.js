import Vue from 'vue/dist/vue.js';
import Beaker from '../../beaker';
import '../../beaker/dist/beaker.css';

Vue.use(Beaker.button);

const app = new Vue({
    el: '#app',
});

// This is just for Beaker

const path = require('path');
const vue = require('rollup-plugin-vue');
const babel = require('rollup-plugin-babel');
const scss = require('rollup-plugin-scss');
const resolve = require('rollup-plugin-node-resolve');

const base = path.resolve(__dirname, './');
const lib = path.resolve(base, 'lib');
const dist = path.resolve(base, 'dist');

const name = 'beaker';

module.exports = {
    entry: path.resolve(lib, 'index.js'),
    moduleName: 'Beaker',
    plugins: [
        vue(),
        babel({
            exclude: 'node_modules/**',
        }),
        resolve(),
        scss({
            output: path.resolve(dist, `${name}.css`),
            indentedSyntax: false,
            outputStyle: 'compressed',
        }),
    ],
    targets: [
        {
            format: 'es',
            dest: path.resolve(dist, `${name}.esm.js`),
            sourceMap: true,
        },
        {
            format: 'umd',
            moduleName: name,
            dest: path.resolve(dist, `${name}.js`),
            sourceMap: true,
        },
    ],
};

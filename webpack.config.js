// This is just for Observatory
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './observatory/app.js',
    devtool: 'source-map',
    output: {
        filename: './observatory/build.js',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: 'babel-loader',
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
        ],
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            template: './observatory/index.html',
        }),
    ],
    devServer: {
        contentBase: './observatory',
        overlay: true,
        watchContentBase: true,
        stats: 'minimal',
        hot: true,
    },
};

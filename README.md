# Beaker
The component API for DataJoe.

Built using bulma.io.

## Using
In your project directory:
```
$ npm i -S bitbucket:datajoe/beaker
```

Then register the components:
```
import Vue from 'vue';

import Beaker from 'beaker';

Vue.use(Beaker);
```

You can also register individual components on an as-needed basis:
```
Vue.use(Beaker.button);
```

If using the ES build, with a proper build system (Rollup or Webpack), tree-shaking will remove all modules not being used, so feel free to import the entire library if your build system supports it.

## Building
This project is built using Rollup. There are two builds:
* `beaker.esm.js` is the ES module build. This requires you to have a build system and allows for tree-shaking.
* `beaker.js` is the UMD build.  

Please choose based on your app environment.

To build the files yourself:  
`npm run build` will build out the files once.  
`npm run watch` will watch the files for any changes and generate new ones on save.

## Observatory
Observatory is the style guide that is built in to Beaker. It lets you see your changes
as you change components within Beaker and provides a platform for documenting said changes.

When running `npm run watch` it will also fire up a local server at `http://localhost:8080`
that hosts the style guide. It includes hot reloading and all that fancy goodness, so develop away!

## Linting
* This project uses ESLint. Please be sure your IDE is hooked up properly.
* Please define all globals in a comment at the top of your file.

For example:
```
/* global window */

// Rest of code here.
```

## Component Structure
Each component is in its own folder in `lib/components`.

Within each folder is two files, `component.vue` and `index.js`.

`component.vue` holds the `<template>` and `<script>` tags with all the logic for that component.

`index.js` holds the code that installs the component. This is pretty boilerplate from component to component.

## Creating a New Component
1. Create a new folder in `lib/components` named after the component.
2. Create a `<component>.vue` file and an `index.js`.
3. Copy the code from an existing `index.js` file and change the necessary parts. Make sure to appropriately prefix the name.
4. Create your component code within `<component>.vue`.
5. Register the component in `lib/index.js` by both importing it and putting it into the `Beaker` constant.
